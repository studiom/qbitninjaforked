﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
#if SERVER
[assembly: AssemblyTitle("QBitNinja")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("QBitNinja")]
[assembly: AssemblyProduct("QBitNinja")]
[assembly: AssemblyCopyright("Copyright © QBitNinja 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("QBitNinja.Tests")]
#else
[assembly: AssemblyTitle("QBitNinja.Client")]
[assembly: AssemblyDescription("Modified original QBitNinja.Client (based on NBitcoin) to support third party API.\r\n\r\nSupported  API:\r\n1. blocktrail.com (get address transactions, unspent outputs)\r\n2. blockdozer.com (broadcasting transaction to the blockchain)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Studio Mobile")]
[assembly: AssemblyProduct("QBitNinja.Client.NETCore.BCC")]
[assembly: AssemblyCopyright("Copyright © Studio Mobile 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
#endif

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
//[assembly: Guid("ed067f8e-05d6-4cc5-82bb-3781be52a6b4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyInformationalVersion("1.0.3.39")]
[assembly: AssemblyVersion("1.0.3.39")]
[assembly: AssemblyFileVersion("1.0.3.39")]

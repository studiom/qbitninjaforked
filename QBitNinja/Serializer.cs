﻿using NBitcoin;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using QBitNinja.Client.Models;
using System.Collections.Generic;
using System;
#if !CLIENT
using QBitNinja.JsonConverters;
#else
using QBitNinja.Client.JsonConverters;
#endif
#if !CLIENT
using System.Net.Http.Formatting;
#endif

#if !CLIENT
namespace QBitNinja
#else
namespace QBitNinja.Client
#endif
{
    public class Serializer
    {
#if !NOJSONNET
		public
#else
		internal
#endif
		static void RegisterFrontConverters(JsonSerializerSettings settings, Network network = null)
        {
			NBitcoin.JsonConverters.Serializer.RegisterFrontConverters(settings, network);
			var unix = settings.Converters.OfType<NBitcoin.JsonConverters.DateTimeToUnixTimeConverter>().First();
			settings.Converters.Remove(unix);
#if !CLIENT
			settings.Converters.Add(new BalanceLocatorJsonConverter());
#endif
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        public static T ToObjectByBlocktrailAPI<T>(string data)
        {
            object result = null;

            JObject jObject = JObject.Parse(data);

            if (typeof(T) == typeof(BalanceModelByBlocktrailAPI))
            {
                List<BalanceOperationByBlocktrailAPI> balanceOperations = new List<BalanceOperationByBlocktrailAPI>();

                foreach (var dataItem in jObject["data"])
                {
                    BalanceOperationByBlocktrailAPI balanceOperation = new BalanceOperationByBlocktrailAPI();
                    balanceOperation.Amount = new Money(decimal.Parse((string)dataItem["estimated_value"]), MoneyUnit.Satoshi);
                    
                    balanceOperation.BlockId = string.IsNullOrEmpty((string)dataItem["block_hash"]) ? 0 : uint256.Parse(dataItem["block_hash"].ToString());
                    balanceOperation.Confirmations = (int)dataItem["confirmations"];
                    balanceOperation.Height = dataItem["block_height"].Value<int?>().GetValueOrDefault();
                    balanceOperation.TransactionId = uint256.Parse((string)dataItem["hash"]);
                    balanceOperation.FirstSeen = dataItem["time"].Value<DateTime>();
                    balanceOperations.Add(balanceOperation);

                    balanceOperation.EstimatedChangeAddress = string.IsNullOrEmpty(dataItem["estimated_change_address"].ToString()) ? "" : dataItem["estimated_change_address"].ToString();
                    balanceOperation.EstimatedValue = new Money(decimal.Parse((string)dataItem["estimated_change"] == null ? "0" : dataItem["estimated_change"].ToString()), MoneyUnit.Satoshi);                                        
                }

                result = new BalanceModelByBlocktrailAPI() { Operations = balanceOperations };
            }

            if (typeof(T) == typeof(List<UnspentOutputByBlocktrailAPI>))
            {
                List<UnspentOutputByBlocktrailAPI> unspentOutputs = new List<UnspentOutputByBlocktrailAPI>();

                foreach (var dataItem in jObject["data"])
                {
                    UnspentOutputByBlocktrailAPI unspentOutput = new UnspentOutputByBlocktrailAPI();
                    unspentOutput.Confirmations = (int)dataItem["confirmations"];
                    unspentOutput.TransactionId = uint256.Parse((string)dataItem["hash"]);

                    string debugStr = (string)dataItem["time"];
                    DateTime dateTime = new DateTime();
                    DateTime.TryParse((string)dataItem["time"], out dateTime);

                    //unspentOutput.FirstSeen = DateTime.Parse((string)dataItem["time"]);
                    unspentOutput.FirstSeen = dateTime;
                    unspentOutputs.Add(unspentOutput);
                    unspentOutput.UnspentCoins.Add(new Coin(new OutPoint(unspentOutput.TransactionId, (int)dataItem["index"]), new TxOut(new Money(decimal.Parse((string)dataItem["value"]), MoneyUnit.Satoshi), new Script((string)dataItem["script"]))));
                }

                result = unspentOutputs;
            }

            if (typeof(T) == typeof(BalanceSummaryByBlocktrailAPI))
            {
                BalanceSummaryDetails balanceSummaryDetails = new BalanceSummaryDetails()
                {
                    Amount = new Money((decimal)jObject["balance"], MoneyUnit.Satoshi),
                    Received = new Money((decimal)jObject["received"], MoneyUnit.Satoshi),
                    TransactionCount = (int)jObject["transactions"]
                };

                result = new BalanceSummaryByBlocktrailAPI()
                {
                    Spendable = balanceSummaryDetails
                };
            }

            return (T)(result);
        }

        public static T ToObject<T>(string data)
        {
            return ToObject<T>(data, null);
        }

        public static T ToObject<T>(string data, Network network)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            RegisterFrontConverters(settings, network);
            return JsonConvert.DeserializeObject<T>(data, settings);
        }

        public static string ToString<T>(T response, Network network)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            RegisterFrontConverters(settings, network);
            return JsonConvert.SerializeObject(response, settings);
        }
        public static string ToString<T>(T response)
        {
            return ToString<T>(response, null);
        }
#if !CLIENT
        public static MediaTypeFormatter JsonMediaTypeFormatter
        {
            get
            {
                var mediaFormat = new JsonMediaTypeFormatter();
                RegisterFrontConverters(mediaFormat.SerializerSettings);
                mediaFormat.Indent = true;
                return mediaFormat;
            }
        }
#endif
    }
}

﻿using NBitcoin;
using NBitcoin.Protocol;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ConsoleTestApp2_.Net_Core_
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new QBitNinjaClient("https://api.blocktrail.com/v1/tbtc/address");

            var balance = client.GetBalanceSummaryByBlocktrailAPI("miXsBzFKBvjHzXsjUH4s48VyfeDVws8S2Q").Result;

            ////////////////////////////////////////////////////////////////////////////////

            var transactions = client.GetTransactionsByBlocktrailAPI("miXsBzFKBvjHzXsjUH4s48VyfeDVws8S2Q").Result.Operations;

            ////////////////////////////////////////////////////////////////////////////////

            BitcoinAddress recepientAddress = new BitcoinPubKeyAddress("mgRoeWs2CeCEuqQmNfhJjnpX8YvtPACmCX", Network.TestNet);
            List<ICoin> coins = new List<ICoin>();

            List<UnspentOutputByBlocktrailAPI> unspentOutputs = client.GetUnspentOutputsByBlocktrailAPI("miXsBzFKBvjHzXsjUH4s48VyfeDVws8S2Q").Result;

            unspentOutputs.ForEach(operation => coins.AddRange(operation.UnspentCoins));

            Mnemonic mnemonic = new Mnemonic("ten cream hotel pottery giraffe increase oil corn distance sell denial check");
            ExtKey extKey = mnemonic.DeriveExtKey();
            Key privateKey = extKey.PrivateKey;
            BitcoinSecret secret = privateKey.GetBitcoinSecret(Network.TestNet);

            var txBuilder = new TransactionBuilder();
            var tx = txBuilder
                .AddCoins(coins)
                .AddKeys(secret)
                .Send(recepientAddress, Money.Parse("0.123")) //mwCwTceJvYV27KXBc3NJZys6CjsgsoeHmf
                .SendFees("0.002")
                .SetChange(secret.PubKey.GetAddress(Network.TestNet))
                .BuildTransaction(true,  SigHash.All);

            //foreach(var input in tx.Inputs)
            //{
            //    input.ScriptSig = secret.ScriptPubKey;
            //}

            //tx.Sign(secret, false);

            client = new QBitNinjaClient("http://tbtc.blockdozer.com/insight-api/tx/send");

            BroadcastResponse broadcastResponse = client.BroadcastByBlockdozerAPI(tx).Result;
            GetTransactionResponse getTxResp = client.GetTransaction(tx.GetHash()).Result;

            //using (var node = Node.Connect(Network.TestNet, "52.73.7.96:3001")) //Connect to the node
            //{
            //    node.VersionHandshake(); //Say hello
            //                             //Advertize your transaction (send just the hash)
            //    node.SendMessage(new InvPayload(InventoryType.MSG_TX, tx.GetHash()));
            //    //Send it
            //    node.SendMessage(new TxPayload(tx));
            //    Thread.Sleep(500); //Wait a bit
            //}

            Console.ReadKey();
        }
    }
}

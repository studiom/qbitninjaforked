﻿using NBitcoin;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using System;
using System.Collections.Generic;

namespace ConsoleTestApp__.Net_Framework_
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new QBitNinjaClient("https://api.blocktrail.com/v1/tbcc/address");            

            var balance = client.GetBalanceSummaryByBlocktrailAPI("mpoHyipjY4b4WAp7PuCF4KYGccQf8RjUDj").Result;

            ////////////////////////////////////////////////////////////////////////////////

            var transactions = client.GetTransactionsByBlocktrailAPI("mpoHyipjY4b4WAp7PuCF4KYGccQf8RjUDj").Result.Operations;

            ////////////////////////////////////////////////////////////////////////////////

            BitcoinAddress recepientAddress = new BitcoinPubKeyAddress("mgRoeWs2CeCEuqQmNfhJjnpX8YvtPACmCX", Network.TestNet);
            List<ICoin> coins = new List<ICoin>();

            List<UnspentOutputByBlocktrailAPI> unspentOutputs = client.GetUnspentOutputsByBlocktrailAPI("mpoHyipjY4b4WAp7PuCF4KYGccQf8RjUDj").Result;

            unspentOutputs.ForEach(operation => coins.AddRange(operation.UnspentCoins));

            Mnemonic mnemonic = new Mnemonic("pitch fuel category tortoise average lend maid diary cloud catalog medal empty");
            ExtKey extKey = mnemonic.DeriveExtKey();
            Key privateKey = extKey.PrivateKey;
            BitcoinSecret secret = privateKey.GetBitcoinSecret(Network.TestNet);

            var txBuilder = new TransactionBuilder();
            var tx = txBuilder
                .AddCoins(coins)
                .AddKeys(secret)
                .Send(recepientAddress, Money.Parse("0.642")) //mwCwTceJvYV27KXBc3NJZys6CjsgsoeHmf
                .SendFees("0.1")
                .SetChange(secret.PubKey.GetAddress(Network.TestNet))
                .BuildTransaction(true, SigHash.ForkId | SigHash.All);

            client = new QBitNinjaClient("http://tbcc.blockdozer.com/insight-api/tx/send");

            BroadcastResponse broadcastResponse = client.BroadcastByBlockdozerAPI(tx).Result;
            GetTransactionResponse getTxResp = client.GetTransaction(tx.GetHash()).Result;

            Console.ReadKey();

        }
    }
}
